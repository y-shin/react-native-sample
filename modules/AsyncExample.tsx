import React from 'react';
import { FlatList, ActivityIndicator, Text, View, Button } from 'react-native';

interface Movie {
  id: string;
  title: string;
  releaseYear: string;
}
interface RespJson {
  title: string;
  description: string;
  movies: Movie[];
}
interface FetchExampleState {
  isLoading: boolean;
  dataSource: Movie[]
}

export default class AsyncExample extends React.Component<{}, FetchExampleState> {
  constructor(props) {
    super(props);
    this.state = { isLoading: false, dataSource: [] }
  }

  async request() {
    try {
      let response = await fetch('https://reactnative.dev/movies.json');
      let responseJson = await response.json()
      return responseJson.movies;
    } catch (error) {
      console.error(error);
    }
  }

  load() {
    this.setState({ isLoading: true })
    this.request().then((movies: Movie[]) => {
      this.setState({ dataSource: movies, isLoading: false })
    }).catch(() => {
      this.setState({ isLoading: false })
    })
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }

    return (
      <View style={{ flex: 1, paddingTop: 20 }}>
        <FlatList
          data={this.state.dataSource}
          renderItem={({ item }) => <Text>{item.title}, {item.releaseYear}</Text>}
          keyExtractor={({ id }, index) => id}
        />
        <Button
          onPress={() => { this.load() }}
          title="Load"
        />
      </View>
    );
  }
}
