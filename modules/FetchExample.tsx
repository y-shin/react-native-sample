import React from 'react';
import { FlatList, ActivityIndicator, Text, View } from 'react-native';

interface Movie {
  id: string;
  title: string;
  releaseYear: string;
}
interface RespJson {
  title: string;
  description: string;
  movies: Movie[];
}
interface FetchExampleState {
  isLoading: boolean;
  dataSource: Movie[]
}

export default class FetchExample extends React.Component<{}, FetchExampleState> {

  constructor(props) {
    super(props);
    this.state = { isLoading: true, dataSource: [] }
  }

  componentDidMount() {
    return fetch('https://reactnative.dev/movies.json')
      .then((response) => response.json())
      .then((responseJson: RespJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson.movies,
        }, function () {

        });

      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }

    return (
      <View style={{ flex: 1, paddingTop: 20 }}>
        <FlatList
          data={this.state.dataSource}
          renderItem={({ item }) => <Text>{item.title}, {item.releaseYear}</Text>}
          keyExtractor={({ id }, index) => id}
        />
      </View>
    );
  }
}
