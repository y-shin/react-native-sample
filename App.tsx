import React from 'react';
import { Button, View } from 'react-native';

import ButtonBasics from './modules/ButtonBasics';
import PizzaTranslator from './modules/PizzaTranslator';
import IScrolledDownAndWhatHappenedNextShockedMe from './modules/IScrolledDownAndWhatHappenedNextShockedMe';
import FlatListBasics from './modules/FlatListBasics';
import SectionListBasics from './modules/SectionListBasics';
import FetchExample from './modules/FetchExample';
import AsyncExample from './modules/AsyncExample';

type ViewType = (
  "App" |
  "PizzaTranslator" |
  "ButtonBasics" |
  "IScrolledDownAndWhatHappenedNextShockedMe" |
  "FlatListBasics" |
  "SectionListBasics" |
  "FetchExample" |
  "AsyncExample"
);

interface AppState {
  viewType: ViewType;
}

export default class App extends React.Component<{}, AppState> {
  constructor(props) {
    super(props);
    this.state = { viewType: 'App' };
  }

  render() {
    if (this.state.viewType == 'PizzaTranslator') {
      return (<PizzaTranslator />)
    } else if (this.state.viewType == 'ButtonBasics') {
      return (<ButtonBasics />)
    } else if (this.state.viewType == 'IScrolledDownAndWhatHappenedNextShockedMe') {
      return (<IScrolledDownAndWhatHappenedNextShockedMe />)
    } else if (this.state.viewType == 'FlatListBasics') {
      return (<FlatListBasics />)
    } else if (this.state.viewType == 'SectionListBasics') {
      return (<SectionListBasics />)
    } else if (this.state.viewType == 'FetchExample') {
      return (<FetchExample />);
    } else if (this.state.viewType == 'AsyncExample') {
      return (<AsyncExample />);
    } else {
      return (
        <View>
          <Button
            onPress={() => { this.setState({ viewType: "PizzaTranslator" }) }}
            title="Pizza"
          />
          <Button
            onPress={() => { this.setState({ viewType: "ButtonBasics" }) }}
            title="Buttons"
          />
          <Button
            onPress={() => { this.setState({ viewType: "IScrolledDownAndWhatHappenedNextShockedMe" }) }}
            title="Scrolls"
          />
          <Button
            onPress={() => { this.setState({ viewType: "FlatListBasics" }) }}
            title="FlatList"
          />
          <Button
            onPress={() => { this.setState({ viewType: "SectionListBasics" }) }}
            title="SectionList"
          />
          <Button
            onPress={() => { this.setState({ viewType: "FetchExample" }) }}
            title="Fetch"
          />
          <Button
            onPress={() => { this.setState({ viewType: "AsyncExample" }) }}
            title="Async"
          />
        </View>
      );
    }
  }
}
